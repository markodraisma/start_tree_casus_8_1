package com.vijfhart.casus.tree;
public interface Node<T extends Node<T>> {

  T getParent();
  void setParent(T parent);

}
